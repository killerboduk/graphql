const express = require('express');
const bodyParser = require('body-parser');
const graphqlHttp = require('express-graphql');
const { buildSchema } = require('graphql');
const mongoose = require('mongoose');

const app  = express();
app.use(bodyParser.json());
const path = require("path");

const db = require("./db");
const collection = "todo";

db.connect((err) =>{
    if(err){
        console.log("unable to connect to db");
        process.exit(1);
    }
    else{
        app.listen(2000,()=>{
            console.log("connected to db");
        });
    }
})


app.get('/',(req,res)=>{
    res.sendFile(path.join(__dirname,'index.html'));
});

// read
app.get('/getTodos',(req,res)=>{
    // get all Todo documents within our todo collection
    // send back to user as json
    db.getDB().collection(collection).find({}).toArray((err,documents)=>{
        if(err)
            console.log(err);
        else{
            res.json(documents);
        }
    });
});
// app.get('/find/:id',(req,res)=>{
//     const todoID = req.params.id;
//     db.getDB().collection(collection).find({_id: db.getPrimaryKey(todoID)}).toArray((err,documents)=>{
//         if(err)
//             console.log(err);
//         else{
//             res.json(documents);
//         }
//     });
// });
// app.get('/update/:id',(req,res)=>{
//     const todoID = req.params.id;
//     var id = {_id: db.getPrimaryKey(todoID)};
//     var newvalues = { $set: {todo: "sit like shit" } };
//     db.getDB().collection(collection).updateOne(id, newvalues, function(err, documents) {
//         if(err)
//             console.log(err);
//         else{
//             res.json(documents);
//         }
//     });
// });
// app.get('/updateinput/:id',(req,res)=>{
//     const todoID = req.params.id;
//     var id = {_id: db.getPrimaryKey(todoID)};
//     var newvalues = { $set: req.body };
//     db.getDB().collection(collection).updateOne(id, newvalues, function(err, documents) {
//         if(err)
//             console.log(err);
//         else{
//             res.json(documents);
//         }
//     });
// });
app.get('/insert',(req,res)=>{
    var myobj = req.body ;
    db.getDB().collection(collection).insertOne(myobj, function(err, documents) {
        if(err)
            console.log(err);
        else{
            res.json(documents);
        }
    });
});
app.get('/delete/:id',(req,res)=>{
        const todoID = req.params.id;
        var id = {_id: db.getPrimaryKey(todoID)};
        db.getDB().collection("customers").deleteOne(id, function(err, documents) {
        if(err)
            console.log(err);
        else{
            res.json(documents);
        }
    });
});


app.get('/getcustomers',(req,res)=>{
    // get all Todo documents within our todo collection
    // send back to user as json
    db.getDB().collection("customers").find({}).toArray((err,documents)=>{
        if(err)
            console.log(err);
        else{
            res.json(documents);
        }
    });
});

app.get('/getusers',(req,res)=>{

    db.getDB().collection("users").find({username:"greg"}).toArray((err,documents)=>{
        if(err)
            console.log(err);
        else{
            res.json(documents);
        }
    });
});



const events = [];

app.use(
    '/graphql'
    ,graphqlHttp({
        schema: buildSchema(`
 
        
        
        type CustomerEvent{
            name: String!
            address: String!
        }
        
        input CustomerEventInput{
            name: String!
            address: String!
        }
        
        
        
        type RootQuery{
            events: [String!]!
        }
    
        type RootMutation{
            getCustomer(customerInput: CustomerEventInput): CustomerEvent
        }
    
        schema {
            query: RootQuery
            mutation: RootMutation
        } 
    `),
        rootValue:{
            events: () =>{
                var result = '';
                db.getDB().collection("customers").find({}).toArray((err,documents)=>{
                    if(err)
                        console.log(err);
                    else{

                    }
                });

                console.log(result);
            },
            getCustomer: (args) =>{
                const event = {
                    name: args.customerInput.name,
                    address: args.customerInput.address,
                };

                db.getDB().collection("customers").insertOne(event, function(err, documents) {
                    if(err)
                        console.log(err);
                    else{

                    }
                });
                return event;
            }
        },
        graphiql: true
    }));





//app.listen(2000);